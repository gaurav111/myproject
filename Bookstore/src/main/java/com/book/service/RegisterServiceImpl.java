package com.book.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.book.dao.RegisterDAO;
import com.book.model.Admin;
import com.book.model.Book;
import com.book.model.Register;

@Service
@Transactional
public class RegisterServiceImpl implements RegisterService {
    @Autowired
    private RegisterDAO registerDAO;

	@Override
	public void addRegister(Register register) {
		// TODO Auto-generated method stub
		registerDAO.addRegister(register);
		
	}

	@Override
	public Admin getAdmin(String username) {
		// TODO Auto-generated method stub
		 return registerDAO.getAdmin(username);
	}

	@Override
	public void addBook(Book book) {
		// TODO Auto-generated method stub
		registerDAO.addBook(book);
		
	}

	@Override
	public Register getUser(String email) {
		// TODO Auto-generated method stub
		return registerDAO.getUser(email);	}

	@Override
	public List<Book> getAllBooks() {
		// TODO Auto-generated method stub
		return registerDAO.getAllBooks();	}

	@Override
	public Book showSingleBook(int bid) {
		// TODO Auto-generated method stub
		return (Book) registerDAO.showSingleBook(bid);
		
	}

	@Override
	public void saveOrder(Book book, Register register) {
		// TODO Auto-generated method stub
		
		registerDAO.saveOrder(book,register);
		
	}
	

}
