package com.book.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Register implements Serializable {
	private static final long serialVersionUID = -3465813074586302847L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int rid;
 
    @Column(nullable = false) 
    private String name;
 
    @Column
    private String email;
 


	@Column
    private String address;
 
    @Column
    private String password;
 
    public int getRid() {
		return rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	} 
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
 
    public String getAddress() {
        return address;
    }
 
    public void setAddress(String address) {
        this.address = address;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }


}
