package com.book.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table
public class Orders implements Serializable {

	private static final long serialVersionUID = -3465813074586302847L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int oid;
	
	@Column
	private String orderId;
	
	@Column
	private Date orderdate;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bid")
    private Book book;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "rid")
	private Register register;
	
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Date getOrderdate() {
		return orderdate;
	}
	public void setOrderdate(Date orderdate) {
		this.orderdate = orderdate;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public Register getRegister() {
		return register;
	}
	public void setRegister(Register register) {
		this.register = register;
	}
}
