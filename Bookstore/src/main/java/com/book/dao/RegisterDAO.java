package com.book.dao;

import java.util.List;

import com.book.model.Admin;
import com.book.model.Book;
import com.book.model.Register;

public interface RegisterDAO {
	public void addRegister(Register register);

	public Admin getAdmin(String username);

	public void addBook(Book book);

	public Register getUser(String email);

	public List<Book> getAllBooks();

	public Book showSingleBook(int bid);

	public void saveOrder(Book book, Register register);

}
