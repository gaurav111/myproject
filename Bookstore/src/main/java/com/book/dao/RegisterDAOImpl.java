package com.book.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.SystemPropertyUtils;

import com.book.controller.HU;
import com.book.model.Admin;
import com.book.model.Book;
import com.book.model.Orders;
import com.book.model.Register;

@Repository
public class RegisterDAOImpl implements RegisterDAO {
    @SuppressWarnings("deprecation")
	@Autowired
    private SessionFactory sessionFactory;
    
    Session s;

	@Override
	public void addRegister(Register register) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(register);
	}

	@Override
	public Admin getAdmin(String username) {
		// TODO Auto-generated method stub
		/*return (Admin) sessionFactory.getCurrentSession().get(
                Admin.class, username);*/
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(Admin.class);
		return (Admin)criteria.add(Restrictions.eq("username", username)).uniqueResult();
		
	}

	@Override
	public void addBook(Book book) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(book);
		
	}

	@Override
	public Register getUser(String email) {
		// TODO Auto-generated method stub
		Criteria criteria=sessionFactory.getCurrentSession().createCriteria(Register.class);
		return (Register)criteria.add(Restrictions.eq("email", email)).uniqueResult();
			}

	@Override
	public List<Book> getAllBooks() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from Book")
                .list();	
		}

	@Override
	public Book showSingleBook(int bid) {
		// TODO Auto-generated method stub
		System.out.println("DAO method");
		s=sessionFactory.openSession();
		Book book = (Book) s.load(Book.class, bid);
		
		return book;
		
	}

	@Override
	public void saveOrder(Book book, Register register) {
		// TODO Auto-generated method stub
		
		System.out.println(book);
		System.out.println(register);
		Orders order=new Orders();
		order.setOrderId("1234");
		Date date=new Date();
		order.setOrderdate(date);
		order.setBook(book);
		order.setRegister(register);
		
		s.close();
		sessionFactory.getCurrentSession().saveOrUpdate(order);
		System.out.println("Success");
	}
	
}