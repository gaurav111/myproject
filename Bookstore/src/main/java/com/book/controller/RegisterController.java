package com.book.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.book.model.Admin;
import com.book.model.Book;
import com.book.model.Register;
import com.book.service.RegisterService;


@Controller
public class RegisterController {
    private static final Logger logger = Logger
            .getLogger(RegisterController.class);
    
   public RegisterController() {
System.out.println("RegisterController() ");
}

   @Autowired
   private RegisterService registerService;
   
   @RequestMapping(value = "/")
   public ModelAndView listEmployee(ModelAndView model) throws IOException {
	   
	   /*Register register = new Register();
       model.addObject("register", register);
       model.setViewName("register");*/
	   model.setViewName("home");
       return model;
   }
   
   @RequestMapping(value = "/register")
   public ModelAndView RegisterUser(ModelAndView model) throws IOException {
	   
	   Register register = new Register();
       model.addObject("register", register);
       model.setViewName("register");
	   
       return model;
   }
   @RequestMapping(value = "/login")
   public ModelAndView LoginUser(ModelAndView model) throws IOException {
	   
	   Register register = new Register();
       model.addObject("register", register);
       model.setViewName("login");
	   
       return model;
   }
   @RequestMapping(value = "/adminLogin")
   public ModelAndView adminMethod(ModelAndView model) throws IOException {
	   
	   Admin admin = new Admin();
       model.addObject("admin", admin);
       model.setViewName("admin");
       return model;
   }
   
   @RequestMapping(value = "/adminSuccess", method = RequestMethod.POST)
   public ModelAndView checkAdmin(@ModelAttribute Admin admin1,HttpServletRequest request) {
        
	   String username=request.getParameter("username");
	   Admin admin =registerService.getAdmin(username);
       if(admin!=null){
    	   ModelAndView model = new ModelAndView("AddBooks");
           model.addObject("admin",admin);
           Book book=new Book();
           model.addObject("book", book);
           return model;    
           }else{
        	   ModelAndView model = new ModelAndView("admin");
        	   return model;
                  
           }
         
     
       
       }
@RequestMapping(value = "/registerMember", method = RequestMethod.POST)
   public ModelAndView saveEmployee(@ModelAttribute Register register1) {
        // if employee id is 0 then creating the
           // employee other updating the employee
           registerService.addRegister(register1);
    	   ModelAndView model = new ModelAndView("login");
    	   Register register=new Register();
    	   model.addObject("register", register);
    	  return model;
   }

@RequestMapping(value="/addBooks",method=RequestMethod.POST)
 public ModelAndView addBook(@ModelAttribute Book book){
   
	registerService.addBook(book);
	System.out.println(book);
	return new ModelAndView("AddBooks");
	
}

@RequestMapping(value = "/usersuccess", method = RequestMethod.POST)
public ModelAndView checkAdmin(@ModelAttribute Register register1,HttpServletRequest request) {
     
	   String email=register1.getEmail();
	   Register register =registerService.getUser(email);
    if(register!=null){
    	HttpSession hr=request.getSession();
    	hr.setAttribute("register", register);
 	   ModelAndView model = new ModelAndView("index");
        model.addObject("register",register);
        List<Book> listbook = registerService.getAllBooks();
        model.addObject("listbook", listbook);
        return model;    
        }else{
     	   ModelAndView model = new ModelAndView("login");
     	   return model;
               
        }
    
    }
@RequestMapping(value="/buynow",method=RequestMethod.GET)
public ModelAndView showSingleBook(HttpServletRequest request){
	int bid=Integer.parseInt(request.getParameter("bid"));
	Book book=registerService.showSingleBook(bid);
	HttpSession hs=request.getSession();
	hs.setAttribute("bid", book.getBid());
	hs.setAttribute("bookID", book.getBookId());
	hs.setAttribute("bookname", book.getBookname());
	hs.setAttribute("author", book.getAuthor());
	hs.setAttribute("price", book.getPrice());
	hs.setAttribute("book", book);
	ModelAndView model=new  ModelAndView("showsinglebook");
	model.addObject("book",book);
	return model;
	
	}
@RequestMapping(value="/confirmbook",method=RequestMethod.POST)
public ModelAndView confirmOrder(HttpServletRequest request){
        	
	HttpSession hs=request.getSession();
	Book book=(Book)hs.getAttribute("book");
	
	HttpSession hr=request.getSession();
	Register register=(Register)hs.getAttribute("register");
	
        	System.out.println(book);
            registerService.saveOrder(book,register);
			return new ModelAndView("ThankYou");
        	
	
}



}