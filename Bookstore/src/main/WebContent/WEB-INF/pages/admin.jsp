<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<!-- <form action="adminSuccess" method="post">
<input type="hidden" name="aid">
Username : <input type="text" name="username">
Password : <input type="password" name="password">
<input type="submit" value="Admin Login">
</form> -->

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div align="center">
        <h1>Admin Login Form</h1>
        <form:form action="adminSuccess" method="post" modelAttribute="admin"> 
        <table>
            <form:hidden path="aid"/>
            <tr>
                <td>User Name:</td>
                <td><form:input path="username" /></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><form:password path="password" /></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input type="submit" value="Login"></td>
            </tr>
        </table>
        </form:form>
    </div>
    </body>
</html>