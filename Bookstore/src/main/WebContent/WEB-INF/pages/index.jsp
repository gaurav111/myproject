<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1 style="text-align: center">Index</h1>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
Welcome, <c:out value="${register.name}"></c:out><br><br>
<table border="1" width="100%" align="center">
<tr>
<th>BID</th>
<th>Book ID</th>
<th>Book Name</th>
<th>Author</th>
<th>Book Price</th>
</tr>
<c:forEach items="${listbook}" var="b">
<tr>
<td><c:out value="${b.bid}"></c:out></td>
<td><c:out value="${b.bookId}"></c:out></td>
<td><c:out value="${b.bookname}"></c:out></td>
<td><c:out value="${b.author}"></c:out></td>
<td><c:out value="${b.price}"></c:out></td>
<td><a href="buynow?bid=${b.bid}">Buy Now</a></td>
</c:forEach>
</table>
</body>
</html>