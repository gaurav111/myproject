<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div align="center">
<h1>Add Books</h1>

<form:form id="bookform" action="addBooks" method="post" modelAttribute="book">
<table>
<form:hidden path="bid"/>
<tr>
<td>Enter Book ID</td>
<td><form:input path="bookId"/></td>
</tr>

<tr>
<td>Enter Book Name</td>
<td><form:input path="bookname"/></td>
</tr>

<tr>
<td>Enter Author Name</td>
<td><form:input path="author"/></td>
</tr>

<tr>
<td>Enter Book Price</td>
<td><form:input path="price"/></td>
</tr>

<tr>
                <td colspan="2" align="center"><input type="submit" value="Add Book"></td>
            </tr>
            <tr>
            <td colspan="3" align="center"><input type="button" value="Reset" onclick="myFunction()"></td>
            </tr>

</table>
</form:form>
<script type="text/javascript">
function myFunction() {
	document.getElementById("bookform").reset();
	}
</script>
</div>
</body>
</html>