package controller;

import model.MyStart;

public class MyList {

	public static MyStart first;   /* First Node for List */
	public int length;
	public int max;
	public int min;

	/* Add method to add elements in list*/
	public void add(int value) {
		MyStart node = new MyStart();
		node.value = value;
		if (first == null) {            /* Adding to first position if first == null    */
			first = node;
			length++;

		} else {                        /* Traversing till null object is found*/
			MyStart n = first;
			while (n.scd != null) {
				n = n.scd;
			}
			n.scd = node;               /* If found then storing new element in node */
			length++;
		}

	}

	public void delete(int index) {
		if (index < length) {
			if (index == 0) {           /* If user pass 0 index then deleting first value and assigning to next node */
				first = first.scd;
				length--;
			} else {                    /* else traverse till previous position and assign next elememnt address to it*/   
				MyStart node = first;
				MyStart node1 = null;
				for (int i = 0; i < index - 1; i++) {
					node = node.scd;
				}
				node1 = node.scd;
				node.scd = node1.scd;
				System.out.println("Deleted  : " + node1.value);
				node1 = null;
				length--;
			}
		} else {
			System.out.println("Enter Correct Index,Index over the array size");
		}
	}

	public void MinMax() {

		MyList minmax = new MyList();
		if (length == 0) {                    /* If list is empty */
			System.out.println("Zero Size");
		} else {
			MyStart node = first;
			if (length == 1) {                /* If list have only one value*/
				minmax.max = node.value;
				minmax.min = node.value;
				System.out.println();
				System.out.println("Max is : " + minmax.max);
				System.out.println("Min is : " + minmax.min);
			}

			MyStart node1 = node.scd;

			if (length != 0 && node.value > node1.value) {     /* else assigning min max*/
				minmax.max = node.value;
				minmax.min = node1.value;
			} else if (length != 0) {
				minmax.max = node1.value;
				minmax.min = node.value;
			}

			for (int i = 2; i < length; i++) {                /*Traversing all elements and finding min & max*/
				if (node1.scd.value > minmax.max) {
					minmax.max = node1.scd.value;
				} else if (node1.scd.value < minmax.min) {
					minmax.min = node1.scd.value;
				}
				node1 = node1.scd;
			}
		}
		System.out.println();
		System.out.println("Max is : " + minmax.max);
		System.out.println("Min is : " + minmax.min);
		

	}

	public void print() {                               /* Printing all the elements through Traversing */
        System.out.println();
		if (length == 0) {
			System.out.println("Zero Size");
		} else {
			int i=0;
			System.out.println("Index|Element");
			MyStart node = first;
			while (node.scd != null) {
				System.out.println(i+"    |"+  node.value);
				i++;

				node = node.scd;
			}
			System.out.println(i+"    |"+node.value);
            System.out.println();
			System.out.println("length " + length);
		}
	}

}
