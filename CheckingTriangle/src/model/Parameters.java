package model;

public class Parameters {

	double angleA;
	double angleB;
	double angleC;
	
	String AngleType;
	
	
	double Area;
	
	double Perimeter;
	
	String sideType;

	public double getAngleA() {
		return angleA;
	}

	public void setAngleA(double angleA) {
		this.angleA = angleA;
	}

	public double getAngleB() {
		return angleB;
	}

	public void setAngleB(double angleB) {
		this.angleB = angleB;
	}

	public double getAngleC() {
		return angleC;
	}

	public void setAngleC(double angleC) {
		this.angleC = angleC;
	}

	public String getAngleType() {
		return AngleType;
	}

	public void setAngleType(String angleType) {
		AngleType = angleType;
	}

	public double getArea() {
		return Area;
	}

	public void setArea(double area) {
		Area = area;
	}

	public double getPerimeter() {
		return Perimeter;
	}

	public void setPerimeter(double perimeter) {
		Perimeter = perimeter;
	}

	public String getSideType() {
		return sideType;
	}

	public void setSideType(String sideType) {
		this.sideType = sideType;
	}
	
	
	
}
