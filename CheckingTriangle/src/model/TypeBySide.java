package model;

public class TypeBySide {
	Parameters p=new Parameters();

	/* Method to find Triangle type by sides */
	public Parameters sides(int side1, int side2, int side3) {
		if(side1==side2&&side2==side3){
			System.out.println("Equilateral Triangle");
			p.setSideType("Equilateral Triangle");
			angles(side1,side2,side3);
		}
		if((side1==side2&&side1!=side3&&side2!=side3)||(side2==side3&&side2!=side1&&side3!=side1)||(side1==side3&&side1!=side2&&side3!=side2)){
			System.out.println("Isosceles triangle");
			p.setSideType("Isosceles Triangle");
			angles(side1,side2,side3);
		}
		if(side1!=side2 && side2!=side3 && side1!=side3){
			System.out.println("Scalene Triangle");
			p.setSideType("Scalene Triangle");
			angles(side1,side2,side3);
		}
		
		areaPerimeter(side1,side2,side3);
		return p;
		
		
	}
	
	/* Method to Find Angles */

public void angles(int side1, int side2, int side3) {
		
		

		double angleA = Math.toDegrees(
				Math.acos((Math.pow(side2, 2) + Math.pow(side3, 2) - Math.pow(side1, 2)) / (2 * side2 * side3)));

		angleA=angleA*100;
		angleA=Math.floor(angleA);
		angleA=angleA/100;
		
		p.setAngleA(angleA);

		double angleB = Math.toDegrees(
				Math.acos((Math.pow(side1, 2) + Math.pow(side3, 2) - Math.pow(side2, 2)) / (2 * side1 * side3)));
		angleB=angleB*100;
		angleB=Math.floor(angleB);
		angleB=angleB/100;

		
		p.setAngleB(angleB);

		double angleC = Math.toDegrees(
				Math.acos((Math.pow(side1, 2) + Math.pow(side2, 2) - Math.pow(side3, 2)) / (2 * side1 * side2)));

		angleC=angleC*100;
		angleC=Math.floor(angleC);
		angleC=angleC/100;
		
		p.setAngleC(angleC);

		/* Finding Angle Type triangle */
		if (angleA < 90 && angleB < 90 && angleC < 90) {
			p.setAngleType("Acute");
		}
		if (angleA == 90 || angleB == 90 || angleC == 90) {
		   p.setAngleType("Rigth");
		}
		if (angleA > 90 || angleB > 90 || angleC > 90) {
			p.setAngleType("Obtuse");
		}

	}

/* Method To Find Area and Perimeter */
private void areaPerimeter(int side1, int side2, int side3) {

	double para=side1+side2+side3;
	double area=Math.sqrt((para/2)*(para/2-side1)*(para/2-side2)*(para/2-side3));
	
	area=area*100;
	area=Math.floor(area);
	area=area/100;
	p.setArea(area);
	p.setPerimeter(para);
	
	
}


}
