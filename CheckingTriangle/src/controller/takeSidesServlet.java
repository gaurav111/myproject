package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Parameters;
import model.TypeBySide;

@WebServlet("/takeSidesServlet")
public class takeSidesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/* Getting Parameters of sides into int type */

		PrintWriter out = response.getWriter();
		int side1 = Integer.parseInt(request.getParameter("side1"));
		int side2 = Integer.parseInt(request.getParameter("side2"));
		int side3 = Integer.parseInt(request.getParameter("side3"));

		if ((side1 + side2) > side3 && (side1 + side3) > side2 && (side2 + side3 > side1)) {

			TypeBySide find = new TypeBySide();
			Parameters p = find.sides(side1, side2, side3);

			HttpSession session = request.getSession();
			session.setAttribute("side1", side1);
			session.setAttribute("side2", side2);
			session.setAttribute("side3", side3);

			session.setAttribute("p", p);
			response.sendRedirect("Result.jsp");
		} else {
			response.setContentType("text/html");
			out.print("<h1 style='color:red;font-size:medium'>Sum of 2 sides must greater than 3rd side</h1>");
			request.getRequestDispatcher("sides.jsp").include(request, response);
		}

	}

}
