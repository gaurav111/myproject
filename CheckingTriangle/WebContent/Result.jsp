<%@page import="model.Parameters"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Result Page</title>

<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body style="background-color: silver;">
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<%
		Parameters p = (Parameters) session.getAttribute("p");
		int side1 = (int) session.getAttribute("side1");
		int side2 = (int) session.getAttribute("side2");
		int side3 = (int) session.getAttribute("side3");
		double angle = p.getAngleC();
	%>
	<!-- Directing to welcome.jsp page After click on Exit  -->

	<a
		style="text-decoration: none; font-weight: bold; font-size: large; color: black"
		href="welcome.jsp">Exit</a>

	<!-- Inserting Table to Show Results  -->

	<table border="1" width="100%" style="color: black;">
		<tr style="background-color: grey;">
			<th>Dimension</th>
			<th>Value</th>
		</tr>
		<tr
			style="color: black; text-align: center; background-color: orange; font-size: x-large; font-weight: bolder;">
			<td><c:out value="Triangle Type"></c:out></td>
			<td><c:out value="${p.getSideType()}"></c:out></td>
		</tr>
		<tr style="color: black; text-align: center; background-color: orange">
			<td><c:out value="Side A"></c:out></td>
			<td><c:out value="${side1}"></c:out></td>
		</tr>
		<tr
			style="color: black; text-align: center; background-color: orange;">
			<td><c:out value="Side B"></c:out></td>
			<td><c:out value="${side2}"></c:out></td>
		</tr>
		<tr style="color: black; text-align: center; background-color: orange">
			<td><c:out value="Side C"></c:out></td>
			<td><c:out value="${side3}"></c:out></td>
		</tr>
		<tr style="color: black; text-align: center; background-color: orange">
			<td><c:out value="Area"></c:out></td>
			<td><c:out value="${p.getArea()}"></c:out></td>
		</tr>
		<tr style="color: black; text-align: center; background-color: orange">
			<td><c:out value="Perimeter"></c:out></td>
			<td><c:out value="${p.getPerimeter()}"></c:out></td>
		</tr>
		<tr style="color: black; text-align: center; background-color: orange">
			<td><c:out value="Angle A"></c:out></td>
			<td><c:out value="${p.getAngleA()}"></c:out></td>
		</tr>
		<tr style="color: black; text-align: center; background-color: orange">
			<td><c:out value="Angle B"></c:out></td>
			<td><c:out value="${p.getAngleB()}"></c:out></td>
		</tr>
		<tr style="color: black; text-align: center; background-color: orange">
			<td><c:out value="Angle C"></c:out></td>
			<td><c:out value="${p.getAngleC()}"></c:out></td>
		</tr>
		<tr
			style="color: black; text-align: center; background-color: orange; font-weight: bold;">
			<td><c:out value="Triangle Type (Angles)"></c:out></td>
			<td><c:out value="${p.getAngleType()}"></c:out></td>
		</tr>
	</table>
	<canvas class="myCanvas"> </canvas>


	<!-- Script for Creating Triangle  -->


	<script type="text/javascript">
		var side1 =<%=side1%>;
		var side2 =<%=side2%>;
		var angle =<%=angle%>;
		
		var canvas = document.querySelector('.myCanvas');
		var width = canvas.width = window.innerWidth;
		var height = canvas.height = window.innerHeight;
		var ctx = canvas.getContext('2d');
		ctx.fillStyle = 'rgb(0,0,0)';
		ctx.fillRect(0, 0, width, 500);

		ctx.fillStyle = 'rgb(255, 0, 0)';
		ctx.scale(9.5, 9.5);

		var x1 = 75 + Math.cos(Math.PI * angle / 180) * side2;
		var y1 = 50 - Math.sin(Math.PI * angle / 180) * side2;
		ctx.beginPath();
		ctx.moveTo(75, 50);

		ctx.lineTo(75 + side1, 50);
		ctx.lineTo(x1, y1);
		ctx.lineTo(75, 50);
		ctx.fill();
		
		ctx.fillStyle="white";
		
        ctx.font="1.5px Arial";
		ctx.fillText("A",x1-1,y1);
		ctx.fillText("B",75+side1,50);
		ctx.fillText("C",74,50);
		ctx.font="1.5px Arial";
		ctx.fillText("Side A",74+(side1/2),51);
	</script>

</body>

</html>