<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Enter Sides Values</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body
	style="margin: 0; padding: 0; background: url('image/Triangle2.jpg'); background-size: cover; background-position: center; font-family: sans-serif;">
	<h1
		style="margin: 100px; padding: 0 0 20px; text-align: center; font-size: 24px;color: red;font-family: 'Open Sans Condensed', sans-serif;">
		Welcome ,
		<%
		out.println(request.getParameter("name"));
	%>
	</h1>

<!-- Taking 3 Sides From user in Form with action 'takeSidesServlet'  -->
<div class="loginbox1"
	style=" width: 420px; height: 400px; background: white; color: white; top: 50%; left: 50%; position: absolute; 
	transform: translate(-50%, -50%); box-sizing: border-box; padding: 70px 30px">
	<h1 style="text-align: center; font-size: 22px;color: #797979;font-weight: 100;background: #4D4D4D;text-transform: uppercase;
	    font-family: 'Open Sans Condensed', sans-serif;padding: 20px;margin: -70px -30px 30px -30px;" >Enter Sides</h1>
	<form action="takeSidesServlet" method="get" name="form1">
    <input type="text" name="side1" placeholder="Side 1"  />
    <input type="text" name="side2" placeholder="Side 2" />
    <input type="text" name="side3" placeholder="Side 3" />
    <input type="submit" value="Submit" onclick="return allnumeric()" />
    
    </form>			
</div>
<h1 style="color: white;padding-top: 380px;padding-left: 1000px;font-size: small;">*Enter Values</h1>

<!-- Script for Validating wheter input is numeric not null not 0 -->
<script src="myscript.js"></script>
</body>
</html>