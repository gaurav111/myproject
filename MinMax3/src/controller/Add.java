package controller;

import java.util.Scanner;

import model.MinMax;

public class Add {

	/* Making Static array so can be use outside of a class */
	static int arr[] = new int[1];          
	Scanner s1 = new Scanner(System.in);
	static int j = 0;

	public void add1() {

		String a = s1.next();

		/* Is input is 's' then it will taking input values*/
		if (a.equalsIgnoreCase("s")) {      
			System.out.println("End");
            
			/* Making array and assigning to static array for proper sizing */
			int[] arr2 = new int[arr.length - 1];
            for (int i = 0; i < arr2.length; i++) {
				arr2[i] = arr[i];
			}
			arr = new int[arr.length - 1];
			arr = arr2;
			
			/*Sending array to calculate Min and Max */
			MinMax value = MinMax.getMinMax(arr);
			
			/* Printing Values */
			System.out.println("               Max " + value.max);
			System.out.println("               Min " + value.min);
			System.out.println("               Array Length is " + arr.length);
			for (int i = 0; i < arr.length; i++) {
				System.out.print("              |Index :" + i + "|" + " Element : " + arr[i] + "|");
				System.out.println();
			}
			
			/* Asking user for Want to Add or Delete values from array.For that sending array to AddDelete Class */
			AddAndDelete obj2 = new AddAndDelete();
			arr = obj2.AddOrDelete(arr);
			
			/* If return array != null then asking user to add more values */
			if (arr != null) {
				System.out.println("Add more Values and Enter 's' to Stop");
				add1();
			}
			
        /* If input is Number then sending it for addition */ 
		} else if (a.matches("[0-9]+")) {
			int a1 = Integer.parseInt(a);
			try {
				add(a1);                           /* Catching NullPointer Exception */
			} catch (NullPointerException e) {

				//System.out.println("Thanks");

			}
			
			/* If input is not 's' or not number then Telling user to Enter Correct Credentials */
		} else {
			System.out.println("Please Enter Correct Crendentials");
			add1();
		}

	}

	/* add() method to add input from user in array  */
	private void add(int a) {
		for (int i = j;; i++) {
			if (i < arr.length) {

				arr[i] = a;
			}
			if (i == arr.length) {

				arr[i - 1] = a;
			} else {

				j = j + 1;
				arr = addmore();
				add1();
			}
		}
	}
 
	/* addmore() method to increase size of array as per inputs given by user */
	private int[] addmore() {
		int arr2[] = new int[arr.length + 1];
		for (int i = 0; i < arr.length; i++) {
			arr2[i] = arr[i];
		}
		return arr2;
	}

}
