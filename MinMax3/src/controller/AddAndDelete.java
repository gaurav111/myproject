package controller;

import java.util.Scanner;

import model.MinMax;

public class AddAndDelete {
	Scanner s1 = new Scanner(System.in);

	public int[] AddOrDelete(int[] arr) {
        
		/* 'a' for addition,'d' for delete ,'e' for Exit */
		System.out.println(
				"Do you want to Add or Delete elements: Enter 'a' For add & 'd' for Delete and Enter 'e' for Exit");
		String s = s1.next();
		
		/* If user enters 'a' then increasing size of array using addmore() method and return to add() */
		if (s.equalsIgnoreCase("a")) {

			int[] arr3 = addmore(arr);
			arr = new int[arr3.length];
			arr = arr3;

			return arr;

		}
		
		/* If user enters 'd' then deleting element by user specified index */
		if (s.equalsIgnoreCase("d")) {
			System.out.println("Length of Array " + arr.length);
			System.out.println("Enter Index No of Element to delete");

			while (!s1.hasNextInt()) { // repeat until a number is entered.
				s1.next();
				System.out.println("Enter a number please"); // Telling user it's not a number
	                  }
			int index = s1.nextInt();

			/* Sending array and index to delete method */
			AddAndDelete obj = new AddAndDelete();
			arr = obj.removeTheElement(arr, index);
			
			/* Sending Array to find Min and Max */
			MinMax value = MinMax.getMinMax(arr);
			
			/* Printing */
			System.out.println("               Max " + value.max);
			System.out.println("               Min " + value.min);
			System.out.println("               Array Length is " + arr.length);
			for (int i = 0; i < arr.length; i++) {
				System.out.print("              |Index :" + i + "|" + " Element : " + arr[i] + "|");
				System.out.println();
			}
			
			/* To ask again user for want to add or Delete element */
			arr = AddOrDelete(arr);

			return arr;
          
			/* If User Enter 'e' then Exiting */
		} else if (s.equalsIgnoreCase("e")) {

			return null;
		
			/* If user enter other than 'a' , 'd', 'e' then Alerting User*/
		} else {
			System.out.println("Please Enter Correct Credentials");
			arr = AddOrDelete(arr);
			return arr;
		}

	}
 
	/* Method For Deleting element from Array at user specified index */
	public int[] removeTheElement(int[] arr, int index) {
		// If the array is empty
		// or the index is not in array range
		// return the original array
		if (arr == null || index < 0 || index >= arr.length) {

			return arr;
		}

		// Create another array of size one less
		int[] anotherArray = new int[arr.length - 1];

		// Copy the elements except the index
		// from original array to the other array
		for (int i = 0, k = 0; i < arr.length; i++) {

			// if the index is
			// the removal element index
			if (i == index) {
				continue;
			}

			// if the index is not
			// the removal element index
			anotherArray[k++] = arr[i];
		}

		// return the resultant array
		return anotherArray;

	}

	/* addmore() method to increase size of array */
	private int[] addmore(int[] arr) {
		int arr2[] = new int[arr.length + 1];
		for (int i = 0; i < arr.length; i++) {
			arr2[i] = arr[i];
		}
		return arr2;
	}

}
