package model;

import static org.junit.Assert.*;

public class Test {

	@org.junit.Test
	public void testMinMax() {
	
		int arr[]={1,2,3,4};
		MinMax test=new MinMax();
		MinMax v=test.getMinMax(arr);
		assertEquals(1, v.min);
		assertEquals(4, v.max);
	}
	@org.junit.Test
	public void testMinMax1() {
	
		int arr[]={-1,-2,-3,0};
		MinMax test=new MinMax();
		MinMax v=test.getMinMax(arr);
		assertEquals(-3, v.min);
		assertEquals(0, v.max);
	}

}
