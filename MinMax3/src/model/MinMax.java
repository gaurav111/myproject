package model;

public class MinMax {

	/* Declaring min and max Class Variable */
    public int max;
	public int min;

	public static MinMax getMinMax(int[] arr) {
		MinMax value = new MinMax();

		/* If array length ==1 then returning arr[0] as min & max */
		if (arr.length == 1) {
			value.max = arr[0];
			value.min = arr[0];
			return value;
		}

		/* If array length ==0 then telling user that its a Zero Size Array */
		if (arr.length == 0) {
			System.out.println("Zero Size");

		}

		/* If array length !=1 && !=0 then initializing Min Max */
		if (arr.length != 0 && arr[0] > arr[1]) {
			value.max = arr[0];
			value.min = arr[1];
		} else if (arr.length != 0) {
			value.max = arr[1];
			value.min = arr[0];
		}

		/* Comparing array elements to find min and max ,returning object */
		for (int i = 2; i < arr.length; i++) {
			if (arr[i] > value.max) {
				value.max = arr[i];
			} else if (arr[i] < value.min) {
				value.min = arr[i];
			}
		}
		return value;
	}

}
