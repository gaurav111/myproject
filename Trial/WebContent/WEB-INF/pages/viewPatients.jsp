<%@page import="com.mongodb.DBObject"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home</title>
<style type="text/css">
body {
	background-color: #556677;
}

h1 {
	margin-left: 150px;
	font-size: small;
}

#logout {
	text-decoration: none;
	margin-left: 1800px;
	color: black;
	padding-top:
}

#download {
	text-decoration: none;
	margin-left: 1800px;
	color: black;
	padding-top:
}

table, tr, td, th {
	padding: 2px;
}

a {
	text-decoration: none;
}

table th {
	background-color: #999999;
}

#link {
	text-decoration: none;
	margin-left: 1800px;
	color: black;
}
</style>
</head>
<body>

	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<h1>
		Welcome,Dr.
		<c:out value="${doctor.getName()}"></c:out>
	</h1>

	<a id="logout" href="logout">Logout</a>
	<a id="download" href="download">Export CSV</a>
	<a id="link" href="addpatient">Add Patient</a>


	<input type="text"
		style="border-radius: 10px; width: 250px; margin-top: 0; height: 30px; margin-left: 1400px"
		id="myInput" placeholder="  Search Patient by Name"
		onkeyup="myFunction()" />

	<h1
		style="margin: 0; margin-left: 200px; font-size: x-large; color: black;">Patient
		List</h1>
	<table style="margin-top: 0; border-bottom: solid 1px black;"
		cellpadding="1" cellspacing="1" id="myTable" width="80%"
		align="center">
		<thead>
			<tr class="header">
				<th>ID</th>
				<th>Name</th>
				<th>Address</th>
				<th>age</th>
				<th width="40%">Assigned Doctor</th>
				<th colspan="4" scope="colgroup">Operations</th>
			</tr>
		</thead>

		<c:forEach items="${listPatients}" var="b">

			<tbody>
				<tr>
					<td style="color: white;"><c:out value="${b.getId()}"></c:out></td>
					<td style="color: white;"><c:out value="${b.getName()}"></c:out></td>
					<td style="color: white;"><c:out value="${b.getAddress()}"></c:out></td>
					<td style="color: white;"><c:out value="${b.getAge()}"></c:out></td>
					<td style="color: white;"><c:out
							value="${b.getCurrentdoctor()}"></c:out></td>
					<td><a style="color: white;" href="dCSV?id=${b.getId()}">Download
							(as CSV File)</a></td>
					<td style="color: white;"><c:url var="update"
							value="update?id=${b.getId()}" /><a style="color: white;"
						id="update" href="${update}">Update</a></td>
					<td style="color: white;"><c:url var="delete"
							value="delete?id=${b.getId()}" /><a style="color: white;"
						id="delete" href="${delete}">Delete</a></td>
					<td style="color: white;"><c:url var="prescription"
							value="prescription?id=${b.getId()}" /><a style="color: white;"
						id="prescription" href="${prescription}">Add Prescription</a></td>
				</tr>
			</tbody>
		</c:forEach>
	</table>
	<script type="text/javascript">
		function myFunction() {
			var input, filter, table, tr, td, i, txtValue;
			input = document.getElementById("myInput");
			filter = input.value.toUpperCase();
			table = document.getElementById("myTable");
			tr = table.getElementsByTagName("tr");

			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[1];

				if (td) {
					txtValue = td.textContent || td.innerText;
					if (txtValue.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
						// tr[i].getElementsByTagName("td")[1].style.backgroundColor="yellow";
					} else {
						tr[i].style.display = "none";
						tr[i].getElementsByTagName("td")[1].style.backgroundColor = "none";
					}
				}
			}
		}
	</script>
</body>
</html>