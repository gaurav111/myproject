<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Patient</title>
<style type="text/css">
body {
	background-color: #556677;
}

div {
	width: 600px;
	height: 500px;
	background: #666699;
	color: #fff;
	top: 50%;
	left: 50%;
	position: absolute;
	transform: translate(-50%, -50%);
	box-sizing: border-box;
	padding: 70px 30px;
	border-radius: 10px;
}

#fi {
	margin-bottom: 0px;
	border: none;
	border-bottom: 1px solid #fff;
	background: transparent;
	outline: none;
	height: 40px;
	color: #000000;
	font-size: 16;
}

#save {
	cursor: pointer;
	border: none;
	outline: none;
	height: 40px;
	background: buttonhighlight;
	color: #000000;
	font-size: 18px;
	border-radius: 20px;
	width: 200px;
}
</style>
</head>

<body>
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<div align="center">

		<h1
			style="margin: 0; margin-top: -50px; text-align: center; font-size: 22px;">Add
			Patient</h1>
		<form:form action="addPatient" method="post" modelAttribute="patient"
			name="registration" onsubmit="return formValidation()">
			<table>


				<tr>
					<td id="names">ID:</td>
					<td><form:input id="fi" path="id" name="id" value="${pid}"
							readonly="true" /></td>
				</tr>
				<tr>
					<td>Name:</td>
					<td><form:input id="fi" path="name" name="name"
							required="*Field is required" /></td>
				</tr>
				<tr>
					<td>Gender:</td>
					<td><form:select id="fi" path="gender">
							<form:option value="Male">Male</form:option>
							<form:option value="Female">Female</form:option>
						</form:select></td>
				</tr>
				<tr>
					<td>Age:</td>
					<td><form:input id="fi" path="age" name="age"
							required="*Field is required" /></td>
				</tr>

				<tr>
					<td>Marital Status:</td>
					<td><form:select id="fi" path="maritalstatus">
							<form:option value="Unmarried">Unmarried</form:option>
							<form:option value="Married">Married</form:option>
						</form:select></td>
				</tr>
				<tr>
					<td>Address:</td>
					<td><form:input id="fi" path="address" name="address"
							required="*Field is required" /></td>
				</tr>
				<tr>
					<td>Mobile No :</td>
					<td><form:input id="fi" path="mobile" name="mobile"
							required="*Field is required" /></td>
				</tr>
				<tr>
					<td>Current Doctor :</td>
					<td><form:select id="fi" path="currentdoctor"
							items="${doctorname}" /></td>
				</tr>

				<tr>
					<td style="padding-top: 25px;" colspan="2" align="center"><input
						id="save" type="submit" value="Save Patient"></td>
				</tr>
			</table>
		</form:form>
	</div>

	<a style="text-decoration: none; color: white;" href="searchpatients">View
		Patients</a>

	<script type="text/javascript">
		function formValidation() {
			var uname = document.registration.name;
			var uage = document.registration.age;
			var uadd = document.registration.address;
			var umobile = document.registration.mobile;
			//console.log("Ok");
			if (allLetter(uname)) {
				if (allnumeric(uage)) {
					if (allLetter(uadd)) {
						if(allnumeric(umobile)){
						return true;
						}
					}
				}
			}
			return false;
		}
		function allLetter(uname) {
			var letters = /^[A-Za-z ]+$/;
			if (uname.value.match(letters)) {
				return true;
			} else {
				alert('Field Name,Address must have alphabet characters only');
				uname.focus();
				return false;
			}
		}
		function allnumeric(uage) {
			var numbers =/^[0-9]+$/;
			if (uage.value.match(numbers)) {
				return true;
			} else {
				alert(' Field Id, Age,Mobile must have numeric characters only');
				uage.focus();
				return false;
			}
		}
	</script>

</body>
</html>