<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor Login</title>
<style type="text/css">
body {
	background-color: #556677;
	margin: 0;
	padding: 0;
	background-size: cover;
	background-position: center;
	font-family: sans-serif;
}

div {
	width: 320px;
	height: 320px;
	background: #666699;
	color: #fff;
	top: 50%;
	left: 50%;
	position: absolute;
	transform: translate(-50%, -50%);
	box-sizing: border-box;
	padding: 70px 30px;
	border-radius: 10px;
}

#login {
	cursor: pointer;
	margin-top: 25px;
	border: none;
	outline: none;
	height: 40px;
	background: buttonhighlight;
	color: #000000;
	font-size: 18px;
	border-radius: 20px;
	width: 200px;
}

#register {
	color: white;
	text-decoration: none;
}

#email {
	margin-bottom: 0px;
	border: none;
	border-bottom: 1px solid #fff;
	background: transparent;
	outline: none;
	height: 40px;
	color: #fff;
	font-size: 16;
}

#password {
	margin-bottom: 0px;
	border: none;
	border-bottom: 1px solid #fff;
	background: transparent;
	outline: none;
	height: 40px;
	color: #fff;
	font-size: 16;
	
}
</style>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>

	<div align="center">

		<h1
			style="margin: 0; margin-top: -25px; text-align: center; font-size: 22px;">Doctor
			Login Form</h1>
		<h1
			style="font-size: small; font-weight: bold;; color: red;margin-left: 30px;">
			<c:out  value="${result}"></c:out>
		</h1>

		<form:form action="doctorsuccess" method="post"
			modelAttribute="doctor" name='registration'>
			<table>
				<!--             <form:hidden path="id"/>
 -->
				<tr>
					<td><form:input id="email" path="email" type="email"
							placeholder="Email" required="*Field is required" /></td>
				</tr>
				<tr>
					<td><form:input id="password" path="password" type="password"
							placeholder="Password" required="*Field is required" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input id="login" type="submit"
						value="Login"></td>
				</tr>
			</table>

		</form:form>
		<a id="register" href="registerdoctor">Register</a>

	</div>


</body>
</html>