<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Prescription</title>
<style type="text/css">
body {
	background-color: #556677;
	margin: 0;
	padding: 0;
	background-size: cover;
	background-position: center;
	font-family: sans-serif;
}
div {
	width: 320px;
	height: 320px;
	background: #666699;
	color: #fff;
	top: 50%;
	left: 50%;
	position: absolute;
	transform: translate(-50%, -50%);
	box-sizing: border-box;
	padding: 70px 30px;
	border-radius: 10px;
}
#header{
margin-top: -50px;
font-size:22px;
}
#pre{
margin-bottom: 0px;
	border: none;
	border-bottom: 1px solid #fff;
	background: transparent;
	outline: none;
	height: 40px;
	color: #000000;
	font-size: 16;
	}
	#addpre{
	cursor: pointer;
	border: none;
	outline: none;
	height: 40px;
	background: buttonhighlight;
	color: #000000;
	font-size: 18px;
	border-radius: 20px;
	width: 200px;}

</style>
</head>
<body>

	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

	<div align="center">
		<h1 id="header">Add Prescription</h1>
		<form:form action="addPrescription" method="get"
			modelAttribute="prescription">
			<table>
				<tr>
					<td><form:input id="pre" path="drug" placeholder="Drug Name" required="*Field is required" /></td>
				</tr>
				<tr>
					<td><form:input id="pre" path="usageperday" placeholder="Usage/day" required="*Field is required" /></td>
				</tr>
				<tr>
				<tr>
					<td><form:input id="pre" path="quantity" placeholder="Quantity" required="*Field is required" /></td>
				</tr>
				<tr>
					<td style="padding-top: 25px;" colspan="2" align="center"><input id="addpre" type="submit"
						value="Add"></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>