<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Doctor Register</title>
<style type="text/css">
body {
	background-color: #556677;
	margin: 0;
	padding: 0;
	background-size: cover;
	background-position: center;
	font-family: sans-serif;
}

div {
	width: 400px;
	height: 320px;
	background: #666699;
	color: #fff;
	top: 50%;
	left: 50%;
	position: absolute;
	transform: translate(-50%, -50%);
	box-sizing: border-box;
	padding: 70px 30px;
	border-radius: 10px;
}

#rd {
	margin-bottom: 0px;
	border: none;
	border-bottom: 1px solid #fff;
	background: transparent;
	outline: none;
	height: 40px;
	color: #000000;
	font-size: 16;
}

#brd {
	cursor: pointer;
	margin-top: 25px;
	border: none;
	outline: none;
	height: 40px;
	background: buttonhighlight;
	color: #000000;
	font-size: 18px;
	border-radius: 20px;
	width: 200px;
}
</style>
</head>
<body>
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
	<div align="center">
		<h1
			style="margin: 0; margin-top: -50px; text-align: center; font-size: 22px;">Registration</h1>
		<form:form action="registerDoctor" method="post"
			modelAttribute="doctor" name='registration'
			onSubmit="return formValidation()">
			<table>
				<%-- <form:hidden path="id"/> --%>
				<tr>
					<td>Name:</td>
					<td><form:input id="rd" path="name" placeholder="Name"
							name="name" /></td>
				</tr>
				<tr>
					<td>Department:</td>
					<td><form:select id="rd" path="department">
							<form:option value="Accident and emergency (A&E)">Accident and emergency (A&E)</form:option>
							<form:option value="Cardiology ">Cardiology</form:option>
							<form:option value="Ear nose and throat (ENT) ">Ear nose and throat (ENT)</form:option>
							<form:option value="General surgery">General surgery</form:option>
							<form:option value="Nutrition and dietetics">Nutrition and dietetics</form:option>
							<form:option value="Physiotherapy">Physiotherapy</form:option>
						</form:select></td>
				</tr>
				<tr>
					<td>Email:</td>
					<td><form:input id="rd" path="email" type="email"
							placeholder="Email" name="email" /></td>
				</tr>

				<tr>
					<td>Password:</td>
					<td><form:input id="rd" path="password" type="password"
							placeholder="Password" name="password" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input id="brd" type="submit"
						value="Register"></td>
				</tr>
			</table>
		</form:form>


	</div>
	<a style="margin-left: 1150px; text-decoration: none; color: black;"
		href="loginDoctor">Login</a>

	<script type="text/javascript">
		function formValidation() {
			var uname = document.registration.name;
			var uemail = document.registration.email;
			var passid = document.registration.password;
			if (allLetter(uname)) {
				if (ValidateEmail(uemail)) {
					if (passid_validation(passid)) {
						return true;
					}

				}

			}
			return false;
		}
		function allLetter(uname) {
			var letters = [ a - zA - Z ][a - zA - Z] + [ a - zA - Z ]
			$; ///^[A-Za-z]+$/;
			if (uname.value.match(letters)) {
				return true;
			} else {
				alert('Name must have alphabet characters only');
				uname.focus();
				return false;
			}
		}

		function ValidateEmail(uemail) {
			var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			if (uemail.value.match(mailformat)) {
				return true;
			} else {
				alert("You have entered an invalid email address!Please Check Credentials");
				uemail.focus();
				return false;
			}
		}

		function passid_validation(passid) {
			var passid_len = passid.value.length;
			if (passid_len == 0) {
				alert("Password should not be empty");
				passid.focus();
				return false;
			}
			return true;
		}
	</script>
</body>
</html>