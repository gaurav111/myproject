package com.patientsearch.bean;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/** Collection 'patients' to be created in MongoDB and fields name with its getters & setters */
@Document(collection = "patients")
public class Patient {

	@Id
	private String id;

	private String name;
	private String gender;

	private String age;
	private String maritalstatus;
	private String address;
	private String mobile;
	private String currentdoctor;

	/** Embedded document */
	private List<Prescription> prescription;

	public Patient(String name, String gender, String age, String maritalstatus, String address, String mobile,
			String currentdoctor, List<Prescription> prescription) {
		super();
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.maritalstatus = maritalstatus;
		this.address = address;
		this.mobile = mobile;
		this.currentdoctor = currentdoctor;
		this.prescription = prescription;
	}

	public String getId() {
		return id;
	}

	public void setId(String string) {
		this.id = string;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getMaritalstatus() {
		return maritalstatus;
	}

	public void setMaritalstatus(String maritalstatus) {
		this.maritalstatus = maritalstatus;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCurrentdoctor() {
		return currentdoctor;
	}

	public void setCurrentdoctor(String currentdoctor) {
		this.currentdoctor = currentdoctor;
	}

	public List<Prescription> getPrescription() {
		return prescription;
	}

	public void setPrescription(List<Prescription> prescription) {
		this.prescription = prescription;
	}

	public Patient() {

	}

}
