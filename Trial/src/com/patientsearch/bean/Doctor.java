package com.patientsearch.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/** Collection 'doctors' to be created in MongoDB and fields name with its getters & setters */
@Document(collection = "doctors")
public class Doctor {

	@Id
	private String id;

	private String name;
	private String department;

	private String email;
	private String password;

	public Doctor() {

	}

	public Doctor(String name, String department, String email, String password) {
		super();
		this.name = name;
		this.department = department;
		this.email = email;
		this.password = password;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
