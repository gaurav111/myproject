package com.patientsearch.bean;

import java.util.Date;

/** Document to be embedded with 'patients' collection field Prescription*/
public class Prescription {
	private String drug;
	private String usageperday;
	private String quantity;
	private Date date;

	public String getDrug() {
		return drug;
	}

	public void setDrug(String drug) {
		this.drug = drug;
	}

	public String getUsageperday() {
		return usageperday;
	}

	public void setUsageperday(String usageperday) {
		this.usageperday = usageperday;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public Prescription(String drug, String usageperday, String quantity, Date date) {
		super();
		this.drug = drug;
		this.usageperday = usageperday;
		this.quantity = quantity;
		this.date = date;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Prescription() {
	}

	@Override
	public String toString() {
		return ("Drug Name: " + drug + ",Usage/Day: " + usageperday + ", Quatity: " + quantity + ", Date: " + date);
	}

}
