package com.patientsearch.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.patientsearch.bean.Doctor;
import com.patientsearch.bean.Patient;
import com.patientsearch.bean.Prescription;

/** Class with role of DAO */
@Repository
@Qualifier("doctorDao")
public class DoctorDaoImpl implements DoctorDao {

	/** To inject object dependency implicitly */
	@Autowired(required = true)
	MongoTemplate mongoTemplate;

	/** Collection names */
	final String COLLECTION = "doctors";
	final String COLLECTION2 = "patients";

	/** Create Doctor Method */
	public void create(Doctor doctor) {
		mongoTemplate.insert(doctor);
	}

	/** Create Patient Method */
	@Override
	public void create(Patient patient) {
		mongoTemplate.insert(patient);
	}

	/** Update Doctor Method */
	public void update(Doctor doctor) {
		mongoTemplate.save(doctor);
	}

	/** Delete Doctor Method */
	public void delete(Doctor doctor) {
		mongoTemplate.remove(doctor);
	}

	/** Deletes all Doctor Method */
	public void deleteAll() {
		mongoTemplate.remove(new Query(), COLLECTION);
	}

	/** Finding Doctor with fields email and password return as dbo object */
	public DBObject find(Doctor doctor) {

		BasicDBObject andQuery = new BasicDBObject();
		List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
		obj.add(new BasicDBObject("email", doctor.getEmail()));
		obj.add(new BasicDBObject("password", doctor.getPassword()));
		andQuery.put("$and", obj);

		DBObject where_query = new BasicDBObject();
		where_query.put("email", doctor.getEmail());
		where_query.put("password", doctor.getPassword());
		DBObject dbo = mongoTemplate.getCollection(COLLECTION).findOne(where_query);

		return dbo;
	}

	/** Finding all Doctors from doctors colletion */
	public List<Doctor> findAll() {
		return (List<Doctor>) mongoTemplate.findAll(Doctor.class);
	}

	/** Lists all patients */
	@Override
	public List<Patient> findAllPatients() {
		return (List<Patient>) mongoTemplate.findAll(Patient.class);
	}

	/** Finding Patient */
	@Override
	public List<DBObject> find(Patient patient) {

		List<DBObject> mylist = null;
		BasicDBObject andQuery = new BasicDBObject();
		List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
		obj.add(new BasicDBObject("name", patient.getName()));
		andQuery.put("$and", obj);
		mylist = mongoTemplate.getCollection(COLLECTION2).find(andQuery).toArray();
		return mylist;
	}

	/** Finding patient using id */
	@Override
	public Patient findPatientName(String id) {

		DBObject where_query = new BasicDBObject();
		where_query.put("_id", id);
		Query query = new Query(Criteria.where("_id").is(id));
		Patient patient = mongoTemplate.findOne(query, Patient.class);
		return patient;
	}

	/** Updating Patient */
	@Override
	public void update(Patient patient) {
		mongoTemplate.save(patient);

	}

	/** Deletes Patient */
	@Override
	public void delete(Patient patient) {
		mongoTemplate.remove(patient);
	}

	/** Adding Prescription in Patient Data */
	@Override
	public void addPrescription(Patient patient, Prescription prescription) {
		List<Prescription> list = new ArrayList<Prescription>();
		if (patient.getPrescription() == null) {
			list.add(prescription);
		} else {
			list.addAll(patient.getPrescription());
			list.add(prescription);
		}

		patient.setPrescription(list);
		mongoTemplate.save(patient);
	}

	@Override
	public void downloadCSV(HttpServletResponse response, List<Patient> listPatients) throws IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/csv");

		/** creates mock data */
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", "PatientList.csv");
		response.setHeader(headerKey, headerValue);
		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

		String[] header = { "id", "name", "gender", "age", "maritalstatus", "mobile", "address", "currentdoctor",
				"prescription" };

		csvWriter.writeHeader(header);

		for (Patient aBook : listPatients) {
			csvWriter.write(aBook, header);
		}
		csvWriter.close();
	}

	@Override
	public void downloadPatientCsv(Patient patient, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/csv");

		/** creates mock data */
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", patient.getId() + "_Patient.csv");
		response.setHeader(headerKey, headerValue);
		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

		String[] header = { "id", "name", "gender", "age", "maritalstatus", "mobile", "address", "currentdoctor",
				"prescription" };
		csvWriter.writeHeader(header);
		csvWriter.write(patient, header);
		csvWriter.close();
	}

}
