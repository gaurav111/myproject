package com.patientsearch.dao;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.mongodb.DBObject;
import com.patientsearch.bean.Doctor;
import com.patientsearch.bean.Patient;
import com.patientsearch.bean.Prescription;

/** Doctor DAO inteface*/

public interface DoctorDao {

	public void create(Doctor doctor);

	public void update(Doctor doctor);

	public void delete(Doctor doctor);

	public void deleteAll();

	public DBObject find(Doctor doctor);

	public List<Doctor> findAll();

	public void create(Patient patient);

	public List<Patient> findAllPatients();

	public List<DBObject> find(Patient patient);

	public Patient findPatientName(String patientname);

	public void update(Patient patient);

	public void delete(Patient patient);

	public void addPrescription(Patient patient, Prescription prescription);

	public void downloadCSV(HttpServletResponse response,List<Patient> listPatients) throws IOException;

	public void downloadPatientCsv(Patient patient, HttpServletResponse response) throws IOException;
}
