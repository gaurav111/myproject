package com.patientsearch.services;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mongodb.DBObject;
import com.patientsearch.bean.Doctor;
import com.patientsearch.bean.Patient;
import com.patientsearch.bean.Prescription;
import com.patientsearch.dao.DoctorDao;

/** Class to provide logic */
@Service("doctorService")
//@Transactional
public class DoctorServiceImpl implements DoctorService {

	@Autowired
	DoctorDao doctorDao;
	
	@Override
	public void create(Doctor doctor) {
		doctorDao.create(doctor);
	}

	@Override
	public void update(Doctor doctor) {
		doctorDao.update(doctor);
	}

	@Override
	public void delete(Doctor doctor) {
		doctorDao.delete(doctor);
	}

	@Override
	public List<Doctor> findAll() {
		return doctorDao.findAll();
	}

	@Override
	public DBObject find(Doctor doctor) {
		return doctorDao.find(doctor);
	}

	@Override
	public void deleteAll() {
		doctorDao.deleteAll();
	}

	@Override
	public void create(Patient patient) {
		doctorDao.create(patient);
	}

	@Override
	public List<Patient> findAllPatients() {
		return doctorDao.findAllPatients();
	}

	@Override
	public List<DBObject> find(Patient patient) {
		return doctorDao.find(patient);
	}

	@Override
	public Patient findPatientName(String id) {
		return doctorDao.findPatientName(id);
	}

	@Override
	public void update(Patient patient) {
		doctorDao.update(patient);
	}

	@Override
	public void delete(Patient patient) {
		doctorDao.delete(patient);
	}

	@Override
	public void addPrescription(Patient patient, Prescription prescription) {
		doctorDao.addPrescription(patient, prescription);
	}

	@Override
	public void downloadCSV(HttpServletResponse response,List<Patient> listPatients) throws IOException {
		// TODO Auto-generated method stub
		doctorDao.downloadCSV(response, listPatients);
	}

	@Override
	public void downloadPatientCsv(Patient patient, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		doctorDao.downloadPatientCsv(patient,response);
	}

}
