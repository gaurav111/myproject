package com.patientsearch.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.mongodb.DBObject;
import com.patientsearch.bean.Doctor;
import com.patientsearch.bean.Patient;
import com.patientsearch.bean.Prescription;
import com.patientsearch.services.DoctorService;

/** Controller Class */
@Controller
public class ControllerClass {
	/** To inject object dependency implicitly */
	@Autowired
	DoctorService doctorService;

	public ControllerClass() {
		System.out.println("PatientController() ");
	}

	/** Sending request from / url to Login page for Doctors */
	@RequestMapping(value = "/")
	public ModelAndView listEmployee(ModelAndView model) throws IOException {
		Doctor doctor = new Doctor();
		model.addObject("result", "");
		model.addObject("doctor", doctor);
		model.setViewName("loginDoctor");
		return model;
	}

	/** send request to registerDoctor page */
	@RequestMapping(value = "/registerdoctor")
	public ModelAndView registerDoctor(ModelAndView model) throws IOException {

		Doctor doctor = new Doctor();
		model.addObject("doctor", doctor);
		model.setViewName("registerDoctor");
		return model;
	}

	/**
	 * Creates doctor profile and saving data in MongoDb & redirecting to login
	 * page
	 */

	@RequestMapping(value = "/registerDoctor", method = RequestMethod.POST)
	public ModelAndView saveEmployee(@ModelAttribute Doctor doctor1) {
		doctorService.create(doctor1);

		ModelAndView model = new ModelAndView("loginDoctor");
		Doctor doctor = new Doctor();
		model.addObject("doctor", doctor);
		return model;
	}

	/** Updating patient or creating patient */
	@RequestMapping(value = "/addPatient", method = RequestMethod.POST)
	public ModelAndView addPatient(@ModelAttribute("patient") Patient patient) {
		if (patient.getId() != null && !patient.getId().trim().equals("")) {
			doctorService.update(patient);
			System.out.println("patient updated");
		} else {

			doctorService.create(patient);
			System.out.println("patient created");
		}

		/**
		 * * Sending List of Doctor to addpatient page for more patients to be
		 * added
		 */

		List<Patient> listPatients = doctorService.findAllPatients();

		ModelAndView model = new ModelAndView();
		model.addObject("listPatients", listPatients);
		model.setViewName("viewPatients");

		return model;
	}

	/** Request coming from register page to login page of doctor */
	@RequestMapping(value = "/loginDoctor", method = RequestMethod.GET)
	public ModelAndView LoginUser(ModelAndView model) throws IOException {

		Doctor doctor = new Doctor();
		model.addObject("result", "");
		model.addObject("doctor", doctor);
		model.setViewName("loginDoctor");

		return model;
	}

	/**
	 * Finding doctor on basis of email & password .If found redirecting doctor
	 * to viewpatients page or else again on same login page
	 */

	@RequestMapping(value = "/doctorsuccess")
	public ModelAndView loginsuccess(@ModelAttribute Doctor doctor, HttpServletResponse response) throws IOException {
		DBObject dbo = doctorService.find(doctor);

		if (dbo != null) {

			ModelAndView model = new ModelAndView("viewPatients");
			model.addObject("result", "");
			Patient patient = new Patient();
			model.addObject("patient", patient);
			List<Patient> listPatients = doctorService.findAllPatients();
			model.addObject("listPatients", listPatients);
			Doctor doctor1 = new Doctor();
			doctor1.setName(dbo.get("name").toString());

			model.addObject("doctor", doctor1);
			model.addObject("result", "");
			return model;
		} else {
			ModelAndView model = new ModelAndView("loginDoctor");
			model.addObject("result", "Invalid Email or Password");
			return model;
		}

	}

	/** Sending doctor to addpatient page */
	@RequestMapping(value = "/addpatient", method = RequestMethod.GET)
	public ModelAndView addPatient() throws IOException {

		ModelAndView model = new ModelAndView("addpatient");
		Patient patient = new Patient();
		model.addObject("patient", patient);
		List<Patient> listPatients = doctorService.findAllPatients();
		if (listPatients.size() == 0) {
			model.addObject("pid", 1);
		} else {
			int pid = Integer.parseInt(listPatients.get(listPatients.size() - 1).getId());
			model.addObject("pid", pid + 1);
		}
		/**
		 * Adding list of doctor names in model to be used in addpatient form
		 */
		List<Doctor> listdoctor = doctorService.findAll();
		List<String> doctorname = new ArrayList<String>();
		for (int i = 0; i < listdoctor.size(); i++) {
			doctorname.add(
					"Name : " + listdoctor.get(i).getName() + " Specialization : " + listdoctor.get(i).getDepartment());
		}
		model.addObject("doctorname", doctorname);

		return model;

	}

	/**
	 * * Request coming from addpatient page to viewPatients page. Adding list
	 * of patient
	 */

	@RequestMapping(value = "/searchpatients")
	public ModelAndView searchPatients(ModelAndView model, @ModelAttribute Patient patient) throws IOException {

		List<Patient> listPatients = doctorService.findAllPatients();

		model.addObject("listPatients", listPatients);
		model.setViewName("viewPatients");

		return model;
	}

	/**
	 * Download using Swing Framework: Sending download request for patient with
	 * id to Download class and then redirecting again to viewPatients page.
	 * 
	 * 
	 * @RequestMapping(value = "/dCSV", method = RequestMethod.GET) public
	 *                       ModelAndView DownloadCSV(ModelAndView model,
	 *                       HttpServletRequest request,HttpServletResponse
	 *                       response) throws IOException, InterruptedException
	 *                       { String id1 = request.getParameter("id");
	 *                       DownloadClass download = new DownloadClass();
	 *                       response.setContentType("application/csv");
	 *                       response.addHeader("Content-Disposition",
	 *                       "attachment; filename="+id1); String
	 *                       result=download.downloadPateintData(id1);
	 *                       if(result.equalsIgnoreCase("done")){
	 *                       model.addObject("result", "Downloaded at Download
	 *                       Folder.Name :"+id1+"_Patient.csv"); }else{
	 *                       model.addObject("result","Sorry File Not
	 *                       Downloaded"); } List<Patient> listPatients =
	 *                       doctorService.findAllPatients();
	 *                       model.addObject("listPatients", listPatients);
	 *                       model.setViewName("viewPatients"); return model; }
	 */

	/** Download using IcsvBeanWriter API */
	@RequestMapping(value = "/dCSV", method = RequestMethod.GET)
	public ModelAndView DownloadCSV(ModelAndView model, HttpServletResponse response, HttpServletRequest request)
			throws IOException {
		String id1 = request.getParameter("id");
		List<Patient> listPatients = doctorService.findAllPatients();
		Patient patient = doctorService.findPatientName(id1);
		doctorService.downloadPatientCsv(patient, response);
		model.addObject("listPatients", listPatients);
		model.setViewName("viewPatients");
		return model;
	}

	/**
	 * * From viewPatient to update patient using patient id and sending found
	 * patient data to addpatient page
	 */

	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public ModelAndView editUser(@RequestParam(value = "id", required = true) String id, ModelAndView model) {
		model.addObject("patient", doctorService.findPatientName(id));
		List<Doctor> listdoctor = doctorService.findAll();

		List<String> doctorname = new ArrayList<String>();
		for (int i = 0; i < listdoctor.size(); i++) {
			doctorname.add(
					"Name : " + listdoctor.get(i).getName() + " Specialization : " + listdoctor.get(i).getDepartment());
		}
		model.addObject("doctorname", doctorname);
		model.setViewName("addpatient");

		return model;
	}

	/**
	 * From viewPatients: delete patient found using id and redirecting to
	 * viewPatients page
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView deletePateint(@RequestParam(value = "id", required = true) String id, ModelAndView model) {
		Patient patient = doctorService.findPatientName(id);
		doctorService.delete(patient);
		List<Patient> listPatients = doctorService.findAllPatients();

		model.addObject("listPatients", listPatients);
		model.setViewName("viewPatients");
		return model;
	}

	/**
	 * From viewPatients: Found patient from id and sending to addPrescription
	 * page with that patient
	 */

	@RequestMapping(value = "/prescription", method = RequestMethod.GET)
	public ModelAndView toPrescription(@RequestParam(value = "id", required = true) String id, ModelAndView model,
			HttpServletRequest request) {
		Patient patient = doctorService.findPatientName(id);
		request.getSession().setAttribute("patient", patient);
		Prescription prescription = new Prescription();
		model.addObject("prescription", prescription);
		model.setViewName("addPrescription");
		return model;
	}

	// Adding prescription for that specific patient
	@RequestMapping(value = "/addPrescription", method = RequestMethod.GET)
	public ModelAndView addPrescription(@ModelAttribute Prescription prescription, ModelAndView model,
			HttpServletRequest request) {
		Patient patient = (Patient) request.getSession().getAttribute("patient");
		prescription.setDate(new Date());
		doctorService.addPrescription(patient, prescription);

		List<Patient> listPatients = doctorService.findAllPatients();
		model.addObject("listPatients", listPatients);
		model.setViewName("viewPatients");

		return model;
	}

	/**
	 * * From viewPatients: to download all patient data and rediecting to
	 * viewpatients page
	 */
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public ModelAndView downloadAll(ModelAndView model, HttpServletResponse response) throws IOException {

		/**
		 * Using Swing Framework: * DownloadClass download = new
		 * DownloadClass(); download.downloadPateintData();
		 * 
		 * ModelAndView model = new ModelAndView(); List<Patient> listPatients =
		 * doctorService.findAllPatients(); model.addObject("result",
		 * "Downloaded at Download Folder.Name :PatientList.csv");
		 * model.addObject("listPatients", listPatients);
		 * model.setViewName("viewPatients"); return model;
		 */
		List<Patient> listPatients = doctorService.findAllPatients();
		doctorService.downloadCSV(response, listPatients);
		model.addObject("listPatients", listPatients);
		model.setViewName("viewPatients");
		return model;

	}

	/** Logout and invalidating session */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpSession session) {
		session.invalidate();
		Doctor doctor = new Doctor();
		ModelAndView model = new ModelAndView();
		model.addObject("doctor", doctor);
		model.setViewName("loginDoctor");
		return model;
	}

}
