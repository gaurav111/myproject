<%@page import="model.MinMaxclass"%>
<%@page import="controller.*"%>
<%@page import="java.io.PrintWriter"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
HttpSession hs=request.getSession();
MinMaxclass value=(MinMaxclass)hs.getAttribute("value");
int []arr2=(int [])hs.getAttribute("arr2");
PrintWriter pw=response.getWriter();
pw.println("max "+value.max);
pw.println("min "+value.min);
hs.setAttribute("arr2", arr2);
request.getRequestDispatcher("head.jsp").include(request, response);


%>
<table border="1" width="100%">
<tr>
<th>Index</th>
<th>Element</th>
</tr>
<c:forEach begin="0" end="${fn:length(arr2) - 1}" var="index">
   <tr>
      <td><c:out value="${index}"></c:out></td>
      <td><c:out value="${arr2[index]}"></c:out></td>
      <td><a href="DeleteServlet?index=${index}">Delete</a></td>
</c:forEach>
</table>
</body>

</body>
</html>