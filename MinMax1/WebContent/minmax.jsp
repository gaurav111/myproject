
<%@page import="model.MinMaxclass"%>
<%@page import="controller.*"%>
<%@ page import = "java.io.*,java.util.*, javax.servlet.*" %>

   <%--  <%@include file="head.jsp" %> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
    <%@include file="head.jsp" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- Getting arr attribute to display in tabular Format  -->

<%
HttpSession hs=request.getSession();
MinMaxclass value=(MinMaxclass)hs.getAttribute("value");
hs.setAttribute("value",value);
int []arr2=(int [])hs.getAttribute("arr2");
PrintWriter pw=response.getWriter();
pw.println("max "+value.max);
pw.println("min "+value.min);
hs.setAttribute("arr2", arr2);
%>
<!-- Array elements in Tabular Format -->
<table border="1" width="100%">
<tr>
<th>Index</th>
<th>Element</th>
</tr>
<c:forEach begin="0" end="${fn:length(arr2) - 1}" var="index">
   <tr>
      <td><c:out value="${index}"></c:out></td>
      <td><c:out value="${arr2[index]}"></c:out></td>
      <td><a href="DeleteServlet?index=${index}">Delete</a></td>
</c:forEach>

</table>


<!-- Asking User to add more Elements in Array -->
Do YouWant to add more values in list
<a style="text-align: center" href="AddValues.jsp">Yes</a>
<a href="ThankYou.jsp">No</a>
</body>
</html>