
    <%@include file="head.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<!-- Invalidating Session and counting Visits -->
<%
HttpSession hs=request.getSession();
hs.invalidate();
%>
<h1 style="color: black;background-color: yellow">Thanks For Using MinMax</h1>
<%
         Integer hitsCount = (Integer)application.getAttribute("hitCounter");
         if( hitsCount ==null || hitsCount == 0 ) {
            /* First Visit */
            hitsCount = 1;
         } else {
        	 /* Return Visit */
            hitsCount += 1;
         }
         application.setAttribute("hitCounter", hitsCount);
      %>
      <center>
         <p>Total number of visits: <%= hitsCount%></p>
      </center>

</body>
</html>