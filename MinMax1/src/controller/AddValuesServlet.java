package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/AddValuesServlet")
public class AddValuesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/* Creating static array for continuously accepting values from user */
	static int[] arr = new int[1];;
	static int j = 0;
	static int v1;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/* Getting input value from addservlet.jsp to store */
		String v=request.getParameter("value");
		if(v.matches("[0-9]+")){
			v1=Integer.parseInt(v);
			add(request, response);
		}else{
			PrintWriter pw=response.getWriter();
			HttpSession hs=request.getSession();
			hs.setAttribute("error", "Enter Only Numeric Values");
			response.sendRedirect("AddValues.jsp");
		}
		
	}

	private static void add(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

/* Storing input values v1 in array ,after size increases beyond limit it goes in else loop and incresing array length*/
		for (int i = j;; i++) {
			if (i < arr.length) {
				System.out.println("i " + i);
				System.out.println(arr.length);
				arr[i] = v1;
				System.out.println(arr[i]);

			} else {
				System.out.println("i " + i);
				j = j + 1;
				arr = addmore();                             /* addmore() method use to increase array length */
				HttpSession hs = request.getSession();
				hs.setAttribute("arr", arr);
				request.getRequestDispatcher("AddValues.jsp").include(request, response);
				break;

			}
		}
	}

	private static int[] addmore() {

		int arr2[] = new int[arr.length + 1];
		for (int i = 0; i < arr.length; i++) {
			arr2[i] = arr[i];
		}
		return arr2;

	}

	}


