package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.MinMaxclass;


@WebServlet("/FindServlet")
public class FindServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		HttpSession hs=request.getSession();

		int[] arr=(int []) hs.getAttribute("arr");
		
		
		int []arr2=new int[arr.length-1];
		
		for(int i=0;i<arr2.length;i++){
			arr2[i]=arr[i];
		}
		for(int i=0;i<arr2.length;i++){
			System.out.println(arr2[i]);
		}
		
		/* Sending array to MinMaxClass for finding Min & Max */
		MinMaxclass value=MinMaxclass.findMinMax(arr2);
		hs.setAttribute("value", value);
		hs.setAttribute("arr2", arr2);
		
		/* Redirecting to minmax.jsp */
		response.sendRedirect("minmax.jsp");
	
	}

}
