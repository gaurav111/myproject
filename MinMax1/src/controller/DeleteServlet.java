package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.MinMaxclass;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		/* Getting index to delete from array */
		int index = Integer.parseInt(request.getParameter("index"));

		/* Getting Old Array */
		HttpSession hs = request.getSession();
		int[] arr2 = (int[]) hs.getAttribute("arr2");

		/* Sending that array to removeTheElement Method */
		arr2 = removeTheElement(arr2, index);

		/* Sending new array for finding Max Min  */
		MinMaxclass value = MinMaxclass.findMinMax(arr2);
		hs.setAttribute("value", value);
		hs.setAttribute("arr2", arr2);
		
		/* Redirecting to minmax2.jsp */
		response.sendRedirect("minmax2.jsp");

	}

	public static int[] removeTheElement(int[] arr2, int index) {

		// If the array is empty
		// or the index is not in array range
		// return the original array
		if (arr2 == null || index < 0 || index >= arr2.length) {

			return arr2;
		}

		// Create another array of size one less
		int[] anotherArray = new int[arr2.length - 1];

		// Copy the elements except the index
		// from original array to the other array
		for (int i = 0, k = 0; i < arr2.length; i++) {

			// if the index is
			// the removal element index
			if (i == index) {
				continue;
			}

			// if the index is not
			// the removal element index
			anotherArray[k++] = arr2[i];
		}

		// return the resultant array
		return anotherArray;

	
	}

}
